---
title: Utilisation des Images
summary: Démonstration de l'utilisation des images dans MkDocs.
authors:
    - Vous-même
date: 2024-06-19
---

# Utilisation des Images

Nous examinons ici comment intégrer et utiliser des images dans MkDocs.

---

## Images

Voici une autre image pour illustrer :

![Image](../../img/img4.png)

---

### Tableaux

Un exemple de tableau simple :

| Nom        | Âge | Ville    |
|------------|-----|----------|
| Alice      | 25  | New York |
| Bob        | 30  | Paris    |
| Charlie    | 27  | Tokyo    |

---

Pour retourner à [Page 1](../page1.md), cliquez [ici](../page1.md).