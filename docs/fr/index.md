# Bienvenue à MkDocs

Pour une documentation complète, visitez [mkdocs.org](https://www.mkdocs.org).

## Commandes

* `mkdocs new [dir-name]` - Crée un nouveau projet.
* `mkdocs serve` - Démarre le serveur de documentation qui se recharge en direct.
* `mkdocs build` - Construit le site de documentation.
* `mkdocs -h` - Affiche un message d'aide et quitte.

## Présentation du projet

    mkdocs.yml # Le fichier de configuration.
    docs/
        index.md # La page d'accueil de la documentation.
        ...       # Autres pages markdown, images et autres fichiers.
