---
title: Syntaxe de base
summary: Les éléments Markdown décrits dans le document de conception original.
authors:
    - Moi
date: 2024-06-19
---

# Titres

# Titre niveau 1
## Titre niveau 2
### Titre niveau 3
#### Titre niveau 4
##### Titre niveau 5
###### Titre niveau 6

Syntaxe alternative

Titre niveau 1
===============
Titre niveau 2
---------------

# Paragraphes

J'aime vraiment utiliser Markdown.

Je pense l'utiliser pour formater tous mes documents à partir de maintenant.

# Sauts de ligne

C'est la première ligne.  
Et voici la deuxième ligne.

# Emphase

# Gras

J'adore le **texte en gras**.

J'adore le __texte en gras__.

L'amour**est**en gras

# Italique

Le texte en italique est le *nec plus ultra*.

Le texte en italique est le _nec plus ultra_.

Un*chat*miaule

# Gras et Italique

Ce texte est ***vraiment important***.

Ce texte est ___vraiment important___.

Ce texte est __*vraiment important*__.

Ce texte est **_vraiment important_**.

C'est vraiment***très***important

# Citations

> Dorothy la suivit à travers plusieurs des belles pièces de son château.

# Citations avec plusieurs paragraphes

> Dorothy la suivit à travers plusieurs des belles pièces de son château.
>
> La Sorcière lui ordonna de nettoyer les casseroles et les marmites, de balayer le sol et de nourrir le feu avec du bois.

# Citations imbriquées

> Dorothy la suivit à travers plusieurs des belles pièces de son château.
>
>> La Sorcière lui ordonna de nettoyer les casseroles et les marmites, de balayer le sol et de nourrir le feu avec du bois.

# Citations avec d'autres éléments

> #### Les résultats trimestriels sont excellents !
>
> - Le chiffre d'affaires était hors norme.
> - Les bénéfices étaient plus élevés que jamais.
>
>  *Tout* se déroule selon **le plan**.

# Listes

Listes ordonnées

1. Premier élément
2. Deuxième élément
3. Troisième élément
4. Quatrième élément

liste

1. Premier élément
1. Deuxième élément
1. Troisième élément
1. Quatrième élément

liste

1. Premier élément
8. Deuxième élément
3. Troisième élément
5. Quatrième élément

liste

1. Premier élément
2. Deuxième élément
3. Troisième élément
    1. Élément indenté
    2. Élément indenté
4. Quatrième élément

# Listes non ordonnées

- Premier élément
- Deuxième élément
- Troisième élément
- Quatrième élément

liste

* Premier élément
* Deuxième élément
* Troisième élément
* Quatrième élément

liste

+ Premier élément
+ Deuxième élément
+ Troisième élément
+ Quatrième élément

liste

- Premier élément
- Deuxième élément
- Troisième élément
  - Élément indenté
  - Élément indenté
- Quatrième élément


# Démarrage d'éléments de liste non ordonnée avec des nombres

- 1968\. Une excellente année !
- Je pense que 1969 était la deuxième meilleure.

# Ajout d'éléments dans les listes

# Paragraphes

* C'est le premier élément de liste.
* Voici le deuxième élément de liste.

    J'ai besoin d'ajouter un autre paragraphe sous le deuxième élément de liste.

* Et voici le troisième élément de liste.

# Citations

* C'est le premier élément de liste.
* Voici le deuxième élément de liste.

    > Une citation serait parfaite sous le deuxième élément de liste.

* Et voici le troisième élément de liste.

# Blocs de code

1. Ouvrez le fichier.
2. Trouvez le bloc de code suivant à la ligne 21 :

        <html>
          <head>
            <title>Test</title>
          </head>

3. Mettez à jour le titre pour correspondre au nom de votre site web.


# Images

1. Ouvrez le fichier contenant la mascotte Linux.
2. Admirez sa beauté.

    ![Tux, la mascotte Linux]( ../img/Tux.png)

3. Fermez le fichier.

# Listes

1. Premier élément
2. Deuxième élément
3. Troisième élément
    - Élément indenté
    - Élément indenté
4. Quatrième élément


# Code

À l'invite de commande, tapez `nano`.

# Échappement des backticks

``Utilisez `code` dans votre fichier Markdown.``

# Blocs de code

    <html>
      <head>
      </head>
    </html>

# Séparateurs horizontaux

***

---

_________________


# Liens

Mon moteur de recherche préféré est [Duck Duck Go](https://duckduckgo.com).

# Ajout de titres

Mon moteur de recherche préféré est [Duck Duck Go](https://duckduckgo.com "Le meilleur moteur de recherche pour la vie privée").

# URLs et adresses email

<https://www.markdownguide.org>

<fake@example.com>

# Formatage des liens

J'adore soutenir **l'[EFF](https://eff.org)**.

C'est *[Guide Markdown](https://www.markdownguide.org)*.

Consultez la section sur [`code`](#code).

# Liens avec style de référence

## Formatage de la première partie du lien

[hobbit-hole][1]

[hobbit-hole] [1]

## Formatage de la deuxième partie du lien

[1]: https://en.wikipedia.org/wiki/Hobbit#Lifestyle
[1]: https://en.wikipedia.org/wiki/Hobbit#Lifestyle "Modes de vie des hobbits"
[1]: https://en.wikipedia.org/wiki/Hobbit#Lifestyle 'Modes de vie des hobbits'
[1]: https://en.wikipedia.org/wiki/Hobbit#Lifestyle (Modes de vie des hobbits)
[1]: <https://en.wikipedia.org/wiki/Hobbit#Lifestyle> "Modes de vie des hobbits"
[1]: <https://en.wikipedia.org/wiki/Hobbit#Lifestyle> 'Modes de vie des hobbits'
[1]: <https://en.wikipedia.org/wiki/Hobbit#Lifestyle> (Modes de vie des hobbits)


## Un exemple mettant les parties ensemble

Dans un trou vivait un hobbit. Pas un trou méchant, sale et humide, rempli de bouts de vers et d'une odeur nauséabonde, ni un trou sec, nu et sablonneux sans rien pour s'asseoir ou manger : c'était un [trou de hobbit](https://en.wikipedia.org/wiki/Hobbit#Lifestyle "Modes de vie des hobbits"), ce qui signifie le confort.


Dans un trou vivait un hobbit. Pas un trou méchant, sale et humide, rempli de bouts de vers et d'une odeur nauséabonde, ni un trou sec, nu et sablonneux sans rien pour s'asseoir ou manger : c'était un [trou de hobbit][1], ce qui signifie le confort.

[1]: <https://en.wikipedia.org/wiki/Hobbit#Lifestyle> "Modes de vie des hobbits"

# Images

![Les montagnes San Juan sont magnifiques !](../img/san-juan-mountains.jpeg "Montagnes San Juan")

# Liens vers les images

[![Un vieux rocher dans le désert](../img/shiprock.jpeg "Shiprock, Nouveau-Mexique par Beau Rogers")](https://www.flickr.com/photos/beaurogers/31833779864/in/photolist-Qv3rFw-34mt9F-a9Cmfy-5Ha3Zi-9msKdv-o3hgjr-hWpUte-4WMsJ1-KUQ8N-deshUb-vssBD-6CQci6-8AFCiD-zsJWT-nNfsgB-dPDwZJ-bn9JGn-5HtSXY-6CUhAL-a4UTXB-ugPum-KUPSo-fBLNm-6CUmpy-4WMsc9-8a7D3T-83KJev-6CQ2bK-nNusHJ-a78rQH-nw3NvT-7aq2qf-8wwBso-3nNceh-ugSKP-4mh4kh-bbeeqH-a7biME-q3PtTf-brFpgb-cg38zw-bXMZc-nJPELD-f58Lmo-bXMYG-bz8AAi-bxNtNT-bXMYi-bXMY6-bXMYv)

# Échappement des caractères

\* Sans le backslash, ce serait une puce dans une liste non ordonnée.

# HTML

Ce **mot** est en gras. Ce <em>mot</em> est en italique.
