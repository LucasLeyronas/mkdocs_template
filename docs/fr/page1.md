---
title: Feuille de triche Markdown
summary: Feuille de triche Markdown
authors:
    - Moi
date: 2024-06-19
---

# Feuille de triche Markdown

Merci de visiter [Le Guide Markdown](https://www.markdownguide.org) !

Cette feuille de triche Markdown offre un aperçu rapide de tous les éléments de syntaxe Markdown. Elle ne peut pas couvrir tous les cas particuliers, donc si vous avez besoin de plus d'informations sur l'un de ces éléments, référez-vous aux guides de référence pour la [syntaxe de base](https://www.markdownguide.org/basic-syntax/) et la [syntaxe étendue](https://www.markdownguide.org/extended-syntax/).

## Syntaxe de base

Ce sont les éléments décrits dans le document de conception original de John Gruber. Toutes les applications Markdown supportent ces éléments.

### Titre

# Titre H1
## Titre H2
### Titre H3

### Gras

**texte en gras**

### Italique

*texte en italique*

### Citation

> citation

### Liste ordonnée

1. Premier élément
2. Deuxième élément
3. Troisième élément

### Liste non ordonnée

- Premier élément
- Deuxième élément
- Troisième élément

### Code

`code`

### Séparateur horizontal

---

### Lien

[Guide Markdown](https://www.markdownguide.org)

### Image

![texte alternatif](https://www.markdownguide.org/assets/images/tux.png)

## Syntaxe étendue

Ces éléments étendent la syntaxe de base en ajoutant des fonctionnalités supplémentaires. Toutes les applications Markdown ne supportent pas nécessairement ces éléments.

### Tableau

| Syntaxe | Description |
| ----------- | ----------- |
| En-tête | Titre |
| Paragraphe | Texte |

### Bloc de code délimité

```
{
  "prénom": "John",
  "nom": "Smith",
  "âge": 25
}
```

### Note de bas de page

Voici une phrase avec une note de bas de page. [^1]

[^1]: Ceci est la note de bas de page.

### ID de titre

### Mon Super Titre {#identifiant-personnalisé}

### Liste de définition

terme
: définition

### Barré

~~Le monde est plat.~~

### Liste de tâches

- [x] Rédiger le communiqué de presse
- [ ] Mettre à jour le site web
- [ ] Contacter les médias

### Emoji

C'est tellement drôle ! :joy:

(Voir aussi [Copier et coller des Emoji](https://www.markdownguide.org/extended-syntax/#copying-and-pasting-emoji))

### Surlignage

J'ai besoin de surligner ces ==mots très importants==.

### Indice

H~2~O

### Exposant

X^2^