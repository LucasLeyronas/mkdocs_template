---
title: Basic Syntax
summary: The Markdown elements outlined in the original design document.
authors:
    - Me
date: 2024-06-19
---

# Headings

# Heading level 1
## Heading level 2
### Heading level 3
#### Heading level 4
##### Heading level 5
###### Heading level 6

Alternate Syntax

Heading level 1
===============
Heading level 2
---------------

# Paragraphs

I really like using Markdown.

I think I'll use it to format all of my documents from now on.

# Breaks

This is the first line.  
And this is the second line.

# Emphasis

# Bold

I just love **bold text**.

I just love __bold text__.

Love**is**bold

# Italic

Italicized text is the *cat's meow*.

Italicized text is the _cat's meow_.

A*cat*meow

# Bold and Italic

This text is ***really important***.

This text is ___really important___.

This text is __*really important*__.

This text is **_really important_**.

This is really***very***important text.

# Blockquotes

> Dorothy followed her through many of the beautiful rooms in her castle.

# Blockquotes with Multiple Paragraphs

> Dorothy followed her through many of the beautiful rooms in her castle.
>
> The Witch bade her clean the pots and kettles and sweep the floor and keep the fire fed with wood.

# Nested Blockquotes

> Dorothy followed her through many of the beautiful rooms in her castle.
>
>> The Witch bade her clean the pots and kettles and sweep the floor and keep the fire fed with wood.

# Blockquotes with Other Elements

> #### The quarterly results look great!
>
> - Revenue was off the chart.
> - Profits were higher than ever.
>
>  *Everything* is going according to **plan**.

# Lists

Ordered Lists

1. First item
2. Second item
3. Third item
4. Fourth item

list

1. First item
1. Second item
1. Third item
1. Fourth item

list

1. First item
8. Second item
3. Third item
5. Fourth item

list

1. First item
2. Second item
3. Third item
    1. Indented item
    2. Indented item
4. Fourth item

# Unordered Lists

- First item
- Second item
- Third item
- Fourth item

list

* First item
* Second item
* Third item
* Fourth item

list

+ First item
+ Second item
+ Third item
+ Fourth item

list

- First item
- Second item
- Third item
  - Indented item
  - Indented item
- Fourth item


# Starting Unordered List Items With Numbers

- 1968\. A great year!
- I think 1969 was second best.

# Adding Elements in Lists

# Paragraphs

* This is the first list item.
* Here's the second list item.

    I need to add another paragraph below the second list item.

* And here's the third list item.

# Blockquotes

* This is the first list item.
* Here's the second list item.

    > A blockquote would look great below the second list item.

* And here's the third list item.

# Code Blocks

1. Open the file.
2. Find the following code block on line 21:

        <html>
          <head>
            <title>Test</title>
          </head>

3. Update the title to match the name of your website.


# Images

1. Open the file containing the Linux mascot.
2. Marvel at its beauty.

    ![Tux, the Linux mascot]( ../img/Tux.png)

3. Close the file.

# Lists

1. First item
2. Second item
3. Third item
    - Indented item
    - Indented item
4. Fourth item


# Code

At the command prompt, type `nano`.

# Escaping Backticks

``Use `code` in your Markdown file.``

# Code Blocks

    <html>
      <head>
      </head>
    </html>

# Horizontal Rules

***

---

_________________


# Links

My favorite search engine is [Duck Duck Go](https://duckduckgo.com).

# Adding Titles

My favorite search engine is [Duck Duck Go](https://duckduckgo.com "The best search engine for privacy").

# URLs and Email Addresses

<https://www.markdownguide.org>

<fake@example.com>

# Formatting Links

I love supporting the **[EFF](https://eff.org)**.

This is the *[Markdown Guide](https://www.markdownguide.org)*.

See the section on [`code`](#code).

# Reference-style Links


## Formatting the First Part of the Link

[hobbit-hole][1]

[hobbit-hole] [1]

## Formatting the Second Part of the Link

[1]: https://en.wikipedia.org/wiki/Hobbit#Lifestyle
[1]: https://en.wikipedia.org/wiki/Hobbit#Lifestyle "Hobbit lifestyles"
[1]: https://en.wikipedia.org/wiki/Hobbit#Lifestyle 'Hobbit lifestyles'
[1]: https://en.wikipedia.org/wiki/Hobbit#Lifestyle (Hobbit lifestyles)
[1]: <https://en.wikipedia.org/wiki/Hobbit#Lifestyle> "Hobbit lifestyles"
[1]: <https://en.wikipedia.org/wiki/Hobbit#Lifestyle> 'Hobbit lifestyles'
[1]: <https://en.wikipedia.org/wiki/Hobbit#Lifestyle> (Hobbit lifestyles)


## An Example Putting the Parts Together

In a hole in the ground there lived a hobbit. Not a nasty, dirty, wet hole, filled with the ends
of worms and an oozy smell, nor yet a dry, bare, sandy hole with nothing in it to sit down on or to
eat: it was a [hobbit-hole](https://en.wikipedia.org/wiki/Hobbit#Lifestyle "Hobbit lifestyles"), and that means comfort.


In a hole in the ground there lived a hobbit. Not a nasty, dirty, wet hole, filled with the ends
of worms and an oozy smell, nor yet a dry, bare, sandy hole with nothing in it to sit down on or to
eat: it was a [hobbit-hole][1], and that means comfort.

[1]: <https://en.wikipedia.org/wiki/Hobbit#Lifestyle> "Hobbit lifestyles"

# Images

![The San Juan Mountains are beautiful!](../img/san-juan-mountains.jpeg "San Juan Mountains")

# Linking Images

[![An old rock in the desert](../img/shiprock.jpeg "Shiprock, New Mexico by Beau Rogers")](https://www.flickr.com/photos/beaurogers/31833779864/in/photolist-Qv3rFw-34mt9F-a9Cmfy-5Ha3Zi-9msKdv-o3hgjr-hWpUte-4WMsJ1-KUQ8N-deshUb-vssBD-6CQci6-8AFCiD-zsJWT-nNfsgB-dPDwZJ-bn9JGn-5HtSXY-6CUhAL-a4UTXB-ugPum-KUPSo-fBLNm-6CUmpy-4WMsc9-8a7D3T-83KJev-6CQ2bK-nNusHJ-a78rQH-nw3NvT-7aq2qf-8wwBso-3nNceh-ugSKP-4mh4kh-bbeeqH-a7biME-q3PtTf-brFpgb-cg38zw-bXMZc-nJPELD-f58Lmo-bXMYG-bz8AAi-bxNtNT-bXMYi-bXMY6-bXMYv)

# Escaping Characters

\* Without the backslash, this would be a bullet in an unordered list.

# HTML

This **word** is bold. This <em>word</em> is italic.


Many Markdown applications allow you to use HTML tags in Markdown-formatted text. This is helpful if you prefer certain HTML tags to Markdown syntax. For example, some people find it easier to use HTML tags for images. Using HTML is also helpful when you need to change the attributes of an element, like specifying the color of text or changing the width of an image.

To use HTML, place the tags in the text of your Markdown-formatted file.

This **word** is bold. This <em>word</em> is italic.
